// The domain entities. These are simple (immutable) value containers.
// Representing these with classes would have reduced the duplication of
// the types that is present here.

export type Repository = {
  readonly name: string
  readonly url: string
  readonly description: string
  readonly owner: string
  readonly ownerImage: string
  readonly stats: RepositoryStats
}

export type RepositoryStats = {
  readonly forks: number
  readonly stars: number
  readonly issues: number
  readonly watchers: number
}

export type Issue = {
  readonly title: string
  readonly url: string
  readonly comments: number
  readonly reporter: string
  readonly reporterImage: string
}

export const Repository = (
  name: string, url: string, description: string,
  owner: string, ownerImage: string,
  stats: RepositoryStats,
): Repository => ({
  name,
  url,
  description,
  owner,
  ownerImage,
  stats,
})

export const RepositoryStats = (
  forks: number, stars: number,
  issues: number, watchers: number
): RepositoryStats => ({
  forks,
  stars,
  issues,
  watchers,
})

export const Issue = (
  title: string, url: string, comments: number,
  reporter: string, reporterImage: string
): Issue => ({
  title,
  url,
  comments,
  reporter,
  reporterImage,
})

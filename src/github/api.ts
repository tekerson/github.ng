import { Repository, RepositoryStats, Issue } from './values'

type ApiUser = {
  login: string
  avatar_url: string
}

/** Repository object, as returned from the API */
type ApiRepository = {
  full_name: string
  html_url: string
  description: string,
  forks_count: number
  stargazers_count: number
  open_issues_count: number
  watchers_count: number
  owner: ApiUser
}

/** The result of a repository search from the API */
export type ApiRepositoryResult = {
  data: {
    items: ApiRepository[],
  }
}

/** An issue, as returned from the API */
type ApiIssue = {
  title: string
  html_url: string
  comments: number
  user: ApiUser
}

/** The result of fetching repository issues from the API */
export type ApiIssueResult = {
  data: ApiIssue[],
}

/** Convert the repository data returned from the github API to a local Repository model  */
export const fromApiRepository = (repo: ApiRepository) => Repository(
  repo.full_name, repo.html_url, repo.description,
  repo.owner.login, repo.owner.avatar_url,
  RepositoryStats(
    repo.forks_count,
    repo.stargazers_count,
    repo.open_issues_count,
    repo.watchers_count
  )
)

/** Convert the issue data returned from the github API to a local Issue model */
export const fromApiIssue = (issue: ApiIssue): Issue => Issue(
  issue.title, issue.html_url, issue.comments,
  issue.user.login, issue.user.avatar_url
)

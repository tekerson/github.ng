import { Repository, Issue } from './values'
import { fromApiIssue, fromApiRepository, ApiRepositoryResult, ApiIssueResult } from './api'

/** Calculate the path to perform a search request */
const searchPath = (root: string, query: string) => `${root}search/repositories?q=${query}`

/** Calculate the path to get repository issues */
const issuePath = (root: string, repo: string) => `${root}repos/${repo}/issues`

/** The interface required to interact with the github HTTP API */
export interface HttpService {
  get(url: string): Promise<any>
}

/** The Github API wrapper */
export default class Github {
  constructor(private root: string, private http: HttpService) { }

  /** Search the github API for repositories matching the given query */
  searchRepositories(query: string): Promise<Array<Repository>> {
    return this.http.get(searchPath(this.root, query))
      .then((result: ApiRepositoryResult) => result.data.items.map(fromApiRepository))
  }

  /** Fetch from the github API issues related to a given repository */
  fetchIssues(repoName: string): Promise<Array<Issue>> {
    return this.http.get(issuePath(this.root, repoName))
      .then((result: ApiIssueResult) => result.data.map(fromApiIssue))
  }
}

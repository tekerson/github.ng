import * as test from 'tape'
import * as td from 'testdouble'

import Github, { HttpService } from './github'
import { Repository, Issue } from './values'
import { ApiRepositoryResult, ApiIssueResult } from './api'

const setup = () => {
  const http: HttpService = td.object(['get'])
  const github = new Github('/', http)
  return {
    http,
    github,
  }
}

test('`searchRepositories` searches for `Repository`s', (t) => {
  const { github, http } = setup()

  const response: ApiRepositoryResult = {
    data: {
      items: [{
        full_name: 'arbitrary/repository',
        html_url: 'http://example.com/arbitrary/repository',
        description: 'a description of the project',
        forks_count: 1,
        stargazers_count: 2,
        open_issues_count: 3,
        watchers_count: 4,
        owner: {
          login: 'someuser',
          avatar_url: 'http://example.com/avatar.png',
        },
      }],
    },
  }

  td.when(http.get('/search/repositories?q=query')).thenResolve(response)

  const repo: Repository = {
    name: 'arbitrary/repository',
    url: 'http://example.com/arbitrary/repository',
    description: 'a description of the project',
    owner: 'someuser',
    ownerImage: 'http://example.com/avatar.png',
    stats: { forks: 1, stars: 2, issues: 3, watchers: 4 },
  }

  github.searchRepositories('query')
    .then((result) => {
      t.deepEqual(result, [repo], 'retrieves the results of the query')
    })
    .catch(t.fail)
    .then(t.end)
})

test('`fetchIssues` fetches the `Issue`s for the Repository', (t) => {
  const { github, http } = setup()

  const response: ApiIssueResult = { data:
    [{
      title: 'Arbitrary Title',
      html_url: 'http://example.com/arbitrary/repository/issues/2004',
      comments: 4,
      user: {
        login: 'someuser',
        avatar_url: 'http://example.com/avatar.png',
      },
    }],
  }
  const issue: Issue = {
    title: 'Arbitrary Title',
    url: 'http://example.com/arbitrary/repository/issues/2004',
    comments: 4,
    reporter: 'someuser',
    reporterImage: 'http://example.com/avatar.png',
  }

  td.when(http.get('/repos/arbitrary/repository/issues')).thenResolve(response)

  github.fetchIssues('arbitrary/repository')
    .then((result) => {
      t.deepEqual(result, [issue], 'retrieves the issues for the repository')
    })
    .catch(t.fail)
    .then(t.end)
})

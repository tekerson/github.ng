import { Repository } from './github.model'

/** Controller for the search results component */
export class ResultsController {
  /** The results of the most recent search */
  public results: Repository[]

  /** Callback for when a repository is selected from the results list */
  public onSelect: ({ repository }: { repository: Repository }) => void

  /** Select the given repository by calling the provided callback */
  select(repository: Repository) {
    this.onSelect({ repository })
  }
}

/** The Search Results component, for displaying the repositories found from github */
export const ResultsComponent: angular.IComponentOptions = {
  controller: [ResultsController],
  bindings: {
    results: '<',
    onSelect: '&',
  },
  template: `
    <md-list>
      <md-list-item
        ng-repeat="repo in $ctrl.results track by repo.name"
        ng-click="$ctrl.select(repo)"
        aria-label="{{repo.name}}"
      >
        <img ng-src="{{repo.ownerImage}}" class="md-avatar" alt="{{repo.owner}}" />
        <md-list-item-text>
          <h3 ng-bind="repo.name"></h3>
        </md-list-item-text>
      </md-list-item>
    </md-list>
    
  `,
}

import { Repository, Issue } from '../github/values'

export interface GithubApi {
  searchRepositories(query: string): Promise<Array<Repository>>
  fetchIssues(repoName: string): Promise<Array<Issue>>
}

// I didn't attach these properties directly to the controller (which is common) as I would 
// prefer to use an immutable object (ImmutableJS Record for example) for this, but it doesn't
// seem worth adding a library at this stage.
type ViewFields = {
  query: string
  repositories: Array<Repository>
  issues: Array<Issue>
  details: (null | Repository)
  loadingRepositories: boolean
  loadingIssues: boolean
  hasSearched: boolean
  hasRepositories: boolean
  hasIssues: boolean
  hasDetails: boolean
  currentTab: number
}

/**
 * Controller for the top-level github component
 *
 * This is the only "smart" component in the system, it has access to the service and stores
 * state in the form of the view model. All other components are "dumb" and simply render
 * based on their input.
 */
export class GithubController {
  /** view "state" model, to be used in the template for rendering */
  model: ViewFields

  constructor(private github: GithubApi, private mdToast: any) {
    // Start with an empty/default model (i.e. no data loaded)
    this.model = {
      query: '',
      repositories: [],
      issues: [],
      details: null,
      loadingRepositories: false,
      loadingIssues: false,
      hasSearched: false,
      hasRepositories: false,
      hasIssues: false,
      hasDetails: false,
      currentTab: 0,
    }
  }

  /** Get search results for a query from the github service and expose it on the view */
  search(query: string): Promise<void> {
    this.model.loadingRepositories = true
    this.model.query = query
    return this.github.searchRepositories(query)
      .then((repositories) => {
        this.model.repositories = repositories
        this.model.hasSearched = true
        // if there are repositories, display the details for the first one
        if (repositories.length > 0) {
          this.model.details = repositories[0]
          this.model.hasRepositories = true
          this.model.hasDetails = true
        } else {
          this.model.details = null
          this.model.hasRepositories = false
          this.model.hasDetails = false
        }
      }, () => {
        this.showError('An error occurred while fetching the results of your search')
      })
      .then(() => {
        this.model.loadingRepositories = false
      })
  }

  /** Expose the details for the provided repository on the view */
  showDetails(repo: Repository) {
    this.model.currentTab = 0
    if (this.model.details && this.model.details.name === repo.name) {
      return
    }
    this.model.details = repo
    this.model.issues = []
  }

  /** Get issues for a repo from the github service and expose it on the view */
  getIssues(repoName: string): Promise<void> {
    this.model.loadingIssues = true
    return this.github.fetchIssues(repoName)
      .then((issues) => {
        this.model.issues = issues
        this.model.hasIssues = (issues.length > 0)
      }, () => {
        this.showError(`An error occurred while fetching the issues for ${repoName} repository`)
        this.model.currentTab = 0
      })
      .then(() => {
        this.model.loadingIssues = false
      })
  }

  /** Popup a notification when an error occurs */
  showError(error: string) {
    this.mdToast.show(
      this.mdToast.simple()
        .textContent(error)
        .hideDelay(3000)
    );
  }
}

export const GithubComponent: angular.IComponentOptions = {
  controller: ['Github.Service', '$mdToast', GithubController],
  templateUrl: '/src/components/github.component.html'
}

/** The Issues component, for displaying the issues related to a repository from github */
export const IssuesComponent: angular.IComponentOptions = {
  bindings: {
    issues: '<',
  },
  template: `
    <md-list flex>
      <md-list-item ng-repeat="issue in $ctrl.issues track by $index"  class="md-2-line" ng-href="{{issue.url}}">
        <img ng-src="{{issue.reporterImage}}" class="md-avatar" alt="{{issue.reporter}}" />
        <div class="md-list-item-text" layout="column">
          <h3 ng-bind="issue.title"></h3>
          <h4 ng-bind="issue.reporter"></h4>
        </div>
        <span class="md-secondary">{{issue.comments}} Comments</span>
      </md-list-item>
    </md-list>
  `,
}

import { Repository } from '../github/values'

/** Controller for the details component */
export class DetailsController {
  /** The git repository details to be displayed */
  public repository: Repository
}

/** Component that displays the details of a repository */
export const DetailsComponent: angular.IComponentOptions = {
  controller: [DetailsController],
  bindings: {
    repository: '<',
  },
  template: `
    <p ng-bind="$ctrl.repository.description"></p>

    <md-grid-list md-cols="4" md-row-height="150px">
      <md-grid-tile>
        <md-grid-tile-header>
          <h3>Forks</h3>
        </md-grid-tile-header>
        <p ng-bind="$ctrl.repository.stats.forks"></p>
      </md-grid-tile>
      <md-grid-tile>
        <md-grid-tile-header>
          <h3>Stars</h3>
        </md-grid-tile-header>
        <p ng-bind="$ctrl.repository.stats.stars">-</p>
      </md-grid-tile>
      <md-grid-tile>
        <md-grid-tile-header>
          <h3>Open Issues</h3>
        </md-grid-tile-header>
        <p ng-bind="$ctrl.repository.stats.issues">-</p>
      </md-grid-tile>
      <md-grid-tile>
        <md-grid-tile-header>
          <h3>Watchers</h3>
        </md-grid-tile-header>
        <p ng-bind="$ctrl.repository.stats.watchers">-</p>
      </md-grid-tile>
    </md-grid-list>
  `
}

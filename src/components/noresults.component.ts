/** The No Search Results component, for displaying a message when no results are available */
export const NoResultsComponent: angular.IComponentOptions = {
  template: `
    <md-content layout-padding>
      <h3>No Results</h3>
      <p>There were no results found for your query. Time to start a new project?</p>
    </md-content>
  `,
}

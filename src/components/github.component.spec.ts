import * as test from 'tape'
import * as td from 'testdouble'

import { GithubController, GithubApi } from './github.component'

const setup = () => {
  const github: GithubApi = td.object(['searchRepositories', 'fetchIssues'])

  const ctrl = new GithubController(github, null)

  return {
    github,
    ctrl,
  }
}

test('`search`', (t: any) => {
  const { github, ctrl } = setup()

  const results = [{}]
  td.when(github.searchRepositories('query')).thenResolve(results)

  ctrl.search('query')
    .then(() => {
      t.equals(ctrl.model.repositories, results, 'exposes the results on model.repositories')
    })
    .catch(t.fail)
    .then(t.end)
})

test('`getIssues`', (t: any) => {
  const { github, ctrl } = setup()

  const results = [{ name: 'repo/name' }]
  td.when(github.fetchIssues('repo/name')).thenResolve(results)

  ctrl.getIssues('repo/name')
    .then(() => {
      t.equals(ctrl.model.issues, results, 'exposes the results on model.issues')
    })
    .catch(t.fail)
    .then(t.end)
})

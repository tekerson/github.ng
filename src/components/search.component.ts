import { Repository } from './github.model'

/** Controller for the search component */
export class SearchController {
  public query: string

  /** Callback to perform a search for the desired query */
  public onSearch: ({ query }: { query: string }) => void

  /** Call the onSearch callback with the given query */
  search(query: string) {
    this.onSearch({ query })
  }
}

/** Component to display the Search form */
export const SearchComponent: angular.IComponentOptions = {
  controller: [SearchController],
  bindings: {
    query: '<',
    onSearch: '&',
  },
  template: `
    <form ng-submit="$ctrl.search($ctrl.query)">
      <md-input-container class="md-block">
        <label>Search</label>
        <input type="text" ng-model="$ctrl.query" md-autofocus />
      </md-input-container>
    </form>
  `,
}

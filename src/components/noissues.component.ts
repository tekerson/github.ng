/** The No Issues component, for displaying a message when no issues are available for the repo */
export const NoIssuesComponent: angular.IComponentOptions = {
  bindings: {
    repositoryName: '<',
  },
  template: `
    <div layout-padding>
      <h3>No Issues</h3>
      <p>There were no issues found for the {{$ctrl.repositoryName}} repository</p>
    </div>
  `,
}

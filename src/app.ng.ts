import { Github } from './github/index'

import { GithubComponent } from './components/github.component'
import { SearchComponent } from './components/search.component'
import { ResultsComponent } from './components/results.component'
import { NoResultsComponent } from './components/noresults.component'
import { IssuesComponent } from './components/issues.component'
import { NoIssuesComponent } from './components/noissues.component'
import { DetailsComponent } from './components/details.component'

// Wire up the injector
angular.module('GithubBrowser', ['ngMaterial'])
  .constant('ApiRoot', 'https://api.github.com/')
  .service('Github.Service', ['ApiRoot', '$http', Github])

  .component('ghGithub', GithubComponent)
  .component('ghSearch', SearchComponent)
  .component('ghResults', ResultsComponent)
  .component('ghNoResults', NoResultsComponent)
  .component('ghIssues', IssuesComponent)
  .component('ghNoIssues', NoIssuesComponent)
  .component('ghDetails', DetailsComponent)
  
  .config(['$mdThemingProvider', ($mdThemingProvider: any) => {
    $mdThemingProvider.theme('default')
    .primaryPalette('brown')
    .accentPalette('amber');
  }])

# Github Browser

This is an example application using the Github API, it is build with AngularJS (1.5), TypeScript
and Angular Material.

## Installation

The source can be cloned from (ironically) bitbicket, and dependencies installed via npm.

```bash
git clone https://bitbucket.org/tekerson/github.ng.git
cd github.ng
npm install
```

## Usage

Running the project should be as easy as starting the server. This will start the `http-server`
server. But, any simple HTTP server serving from the root of the project should be sufficient.

```bash
npm start
open http://localhost:8080/
```

The (rather limited) test suite can also be run with

```bash
npm test
```

## Limitations and Issues

This is an example application only, and is not in any way "production ready". None of the assets
are minified and optimised and all of the compilation is performed on-the-fly in the browser by the
SystemJS loader.

It is not responsive and will likely not work very well on smaller screens. For example,
improvements could be made by hiding the search results, allowing wrapping of the statistics and
the columns in the issue list.

The design is not very pretty, I'm not a designer (in case you can't tell).

## License

All code in this repository is available under the Apache License Version 2.0.
